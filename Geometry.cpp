#include "Geometry.h"



// * * * * * * * * * * * * * * VECTOR * * * * * * * * * * * * * * //

float Jinko::vector::getNorm() {
	return sqrt(pow(this->x, 2) + pow(this->y, 2));
}
float Jinko::vector::vectorProduct(Jinko::vector A) {
	return ((this->x * A.x) + (this->y * A.y));
}
float Jinko::vector::getAngle(Jinko::vector A) {
	if ((this->getNorm() == 0) || (A.getNorm() == 0)) return 0;
	else return acos(((this->x * A.x) + (this->y * A.y)) / (this->getNorm() * A.getNorm()));
}

bool Jinko::vector::operator==(Jinko::vector const& A) {
	return ((this->x == A.x) && (this->y == A.y));
}
bool Jinko::vector::operator==(Jinko::point const& A) {
	return ((this->x == A.x) && (this->y == A.y));
}
Jinko::vector Jinko::vector::operator+(Jinko::vector const& A) {
	Jinko::vector a(this->x + A.x, this->y + A.y);
	return a;
}
Jinko::vector Jinko::vector::operator-(Jinko::vector const& A) {
	Jinko::vector a(this->x - A.x, this->y - A.y);
	return a;
}
Jinko::point Jinko::vector::operator+(Jinko::point const& A) {
	Jinko::point a(this->x + A.x, this->y + A.y);
	return a;
}
Jinko::point Jinko::vector::operator-(Jinko::point const& A) {
	Jinko::point a(this->x + A.x, this->y + A.y);
	return a;
}


void Jinko::vector::normalize() {
	float norm = this->getNorm();
	this->x = x / norm;
	this->y = y / norm;
}

Jinko::vector Jinko::vector::getNormalizedVector() {
	Jinko::vector vec;
	float norm = this->getNorm();
	vec.x = x / norm;
	vec.y = y / norm;
	return vec;
}

Jinko::vector::vector() {
	this->x = 0;
	this->y = 0;
}
Jinko::vector::vector(float x, float y = 0) {
	this->x = x;
	this->y = y;
}
Jinko::vector::vector(point A) {
	this->x = A.x;
	this->y = A.y;
}

// * * * * * * * * * * * * * * POINT * * * * * * * * * * * * * * //

bool Jinko::point::operator==(Jinko::point const& A) {
	if ((this->x == A.x) && (this->y == A.y)) return true;
	else return false;
}
Jinko::point Jinko::point::operator+(Jinko::point const& A) {
	Jinko::point a(this->x + A.x, this->y + A.y);
	return a;
}
Jinko::point Jinko::point::operator-(Jinko::point const& A) {
	Jinko::point a(this->x - A.x, this->y - A.y);
	return a;
}
Jinko::point Jinko::point::operator+(Jinko::vector const& A) {
	Jinko::point a(this->x + A.x, this->y + A.y);
	return a;
}
Jinko::point Jinko::point::operator-(Jinko::vector const& A) {
	Jinko::point a(this->x - A.x, this->y - A.y);
	return a;
}

Jinko::point::point() {
	this->x = 0;
	this->y = 0;
}
Jinko::point::point(float x, float y) {
	this->x = x;
	this->y = y;
}

// * * * * * * * * * * * * * * IPOINT * * * * * * * * * * * * * * //


bool Jinko::Ipoint::operator==(Jinko::point const& A) {
	if ((this->x == A.x) && (this->y == A.y)) return true;
	else return false;
}
Jinko::point Jinko::Ipoint::operator+(Jinko::point const& A) {
	Jinko::point a(this->x + A.x, this->y + A.y);
	return a;
}
Jinko::point Jinko::Ipoint::operator-(Jinko::point const& A) {
	Jinko::point a(this->x - A.x, this->y - A.y);
	return a;
}
Jinko::point Jinko::Ipoint::operator+(Jinko::vector const& A) {
	Jinko::point a(this->x + A.x, this->y + A.y);
	return a;
}
Jinko::point Jinko::Ipoint::operator-(Jinko::vector const& A) {
	Jinko::point a(this->x - A.x, this->y - A.y);
	return a;
}

Jinko::Ipoint::Ipoint() {
	this->x = 0;
	this->y = 0;
}
Jinko::Ipoint::Ipoint(float x, float y) {
	this->x = round(x);
	this->y = round(y);
}
Jinko::Ipoint::Ipoint(Jinko::point point) {
	this->x = round(point.x);
	this->y = round(point.y);
}



// * * * * * * * * * * * * * * LINE * * * * * * * * * * * * * * //

bool Jinko::line::isOverlapping(Jinko::point X) {
	Jinko::vector V(this->B.x - this->A.x, this->B.y - A.y);
	if ((V.x == 0) && (V.y == 0)) return (this->A == X);
	else if (V.x == 0) {
		return (((V.y + this->A.y >= X.y) && (this->A.y <= X.y)) || ((V.y + this->A.y <= X.y) && (this->A.y >= X.y)));
	}
	else if (V.y == 0) {
		return (((V.x + this->A.x >= X.x) && (this->A.x <= X.x)) || ((V.x + this->A.x <= X.x) && (this->A.x >= X.x)));
	}
	else {
		float alpha1 = (float)(X.x / V.x) - (float)(A.x / V.x);
		float alpha2 = (float)(X.y / V.y) - (float)(A.y / V.y);
		return (alpha1 == alpha2);
	}
}
bool Jinko::line::isOverlapping(float x, float y = 0) {
	Jinko::point a(x, y);
	return this->isOverlapping(a);
}

Jinko::line::line() {
	this->A.x = 0;
	this->A.y = 0;
	this->B.x = 0;
	this->B.y = 0;
}
Jinko::line::line(float x1, float y1, float x2, float y2) {
	this->A.x = x1;
	this->A.y = y1;
	this->B.x = x2;
	this->B.y = y2;
}
Jinko::line::line(Jinko::point A, Jinko::point B) {
	this->A = A;
	this->B = B;
}


// * * * * * * * * * * * * * * TRIANGLE * * * * * * * * * * * * * * //

bool Jinko::triangle::isOverlapping(Jinko::point X) {
	Jinko::vector v1(this->B - this->A);
	Jinko::vector v2(this->C - this->A);
	Jinko::vector v3(X - this->A);

	float a = v1.getAngle(v2);
	float b = v1.getAngle(v3);

	if (b <= a) {
		a = v2.getAngle(v1);
		b = v2.getAngle(v3);
		if (b <= a) {
			v1 = this->A - this->B;
			v2 = this->C - this->B;
			v3 = X - this->B;

			a = v1.getAngle(v2);
			b = v1.getAngle(v3);
			if (b <= a) {
				a = v2.getAngle(v1);
				b = v2.getAngle(v3);
				if (b <= a)
					return true;
			}
		}
	}
	return false;
}
bool Jinko::triangle::isOverlapping(float x, float y) {
	Jinko::point A(x, y);
	return this->isOverlapping(A);
}
bool Jinko::triangle::isOverlapping(float x1, float y1, float x2, float y2) {
	Jinko::point A(x1, y1);
	Jinko::point B(x2, y2);
	return this->isOverlapping(A, B);
}
bool Jinko::triangle::isOverlapping(Jinko::point A, Jinko::point B) {
	return (this->isOverlapping(A) && this->isOverlapping(B));
}
bool Jinko::triangle::isOverlapping(Jinko::line L) {
	return (this->isOverlapping(L.A) && this->isOverlapping(L.B));
}

Jinko::triangle::triangle() {
	this->A.x = 0;
	this->A.y = 0;
	this->B.x = 0;
	this->B.y = 0;
	this->C.x = 0;
	this->C.y = 0;
}
Jinko::triangle::triangle(point A, point B, point C) {
	this->A = A;
	this->B = B;
	this->C = C;
}


// * * * * * * * * * * * * * * QUAD * * * * * * * * * * * * * * * //


bool Jinko::quad::isOverlapping(float x, float y) {
	Jinko::triangle pre(this->A, this->B, this->C);
	Jinko::triangle deu(this->A, this->C, this->D);
	return (pre.isOverlapping(x, y) || deu.isOverlapping(x, y));
}
bool Jinko::quad::isOverlapping(Jinko::point X) {
	return this->isOverlapping(X.x, X.y);
}
bool Jinko::quad::isOverlapping(Jinko::line L) {
	return (this->isOverlapping(L.A) && this->isOverlapping(L.B));
}
bool Jinko::quad::isOverlapping(Jinko::triangle T) {
	return (this->isOverlapping(T.A) && this->isOverlapping(T.B) && this->isOverlapping(T.C));
}

bool Jinko::quad::isOverlappingPartially(Jinko::line L) {
	return (this->isOverlapping(L.A) || this->isOverlapping(L.B));
}
bool Jinko::quad::isOverlappingPartially(Jinko::triangle T) {
	return (this->isOverlapping(T.A) || this->isOverlapping(T.B) || this->isOverlapping(T.C));
}


Jinko::quad::quad() {
	this->A.x = 0;
	this->A.y = 0;
	this->B.x = 0;
	this->B.y = 0;
	this->C.x = 0;
	this->C.y = 0;
	this->D.x = 0;
	this->D.y = 0;
}
Jinko::quad::quad(point A, point B, point C, point D) {
	this->A = A;
	this->B = B;
	this->C = C;
	this->D = D;
}