#include "Keyboard.h"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * inputs * * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
//Returns 0 if inactive, 1 if newly inactive, 2 if active, 3 if newly active.
int Agnosia::input::getInputState() {
	if (this->wasActive == false) {
		if (this->isActive == false) {
			return 0;
		}
		else {
			return 3;
		}
	}
	else {
		if (this->isActive == false) {
			return 1;
		}
		else {
			return 2;
		}
	}
}

Agnosia::input::input() {

}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * mouseInputs * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

void Agnosia::mouseInputs::update() {
	for (int i = 0; i < AmountMouseKeys; i++) {
		SHORT test = GetKeyState(this->keys[i].key);
		this->keys[i].wasActive = this->keys[i].isActive;
		if (test % (2 ^ 15) == 1) {
			this->keys[i].isActive = true;
		}
		else {
			this->keys[i].isActive = false;
		}
	}
}

int Agnosia::mouseInputs::getInputState(int which) {
	if (this->keys[which].wasActive == false) {
		if (this->keys[which].isActive == false) {
			return 0;
		}
		else {
			return 3;
		}
	}
	else {
		if (this->keys[which].isActive == false) {
			return 1;
		}
		else {
			return 2;
		}
	}
}

Agnosia::mouseInputs::mouseInputs() {
	this->keys[0].key = VK_LBUTTON;
	this->keys[1].key = VK_MBUTTON;
	this->keys[2].key = VK_RBUTTON;
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * inputBoard * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

void Agnosia::inputBoard::update() {
	for (int i = 0; i < AmountLinekeys; i++) {
		SHORT test = GetKeyState(this->keys[i].key);
		this->keys[i].wasActive = this->keys[i].isActive;
		if (test < 0) {
			this->keys[i].isActive = true;
		}
		else {
			this->keys[i].isActive = false;
		}
	}
	this->mouse.update();
}

int Agnosia::inputBoard::getKeyboardInput(int line) {
	return this->keys[line].getInputState();
}

int Agnosia::inputBoard::getMouseInput(int button) {
	return this->mouse.getInputState(button);
}

Agnosia::inputBoard::inputBoard() {
	this->keys[0].key = AZERTY_Z;
	this->keys[1].key = AZERTY_Q;
	this->keys[2].key = AZERTY_S;
	this->keys[3].key = AZERTY_D;
	for (int i = 4; i < AmountLinekeys; i++)this->keys[i].key = AZERTY_A;
}
