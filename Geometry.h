#pragma once
//#include "JinkoDependencies.h"
#include "Jinko.h"

class Jinko::vector {
public:
	float x;
	float y;

	float getNorm();
	float vectorProduct(Jinko::vector);
	float getAngle(Jinko::vector);

	bool operator==(Jinko::vector const&);
	bool operator==(Jinko::point const&);
	Jinko::vector operator+(Jinko::vector const&);
	Jinko::vector operator-(Jinko::vector const&);
	Jinko::point operator+(Jinko::point const&);
	Jinko::point operator-(Jinko::point const&);

	void normalize();
	Jinko::vector getNormalizedVector();

	vector();
	vector(float, float);
	vector(Jinko::point);
};

class Jinko::point {
public:
	float x;
	float y;

	bool operator==(Jinko::point const&);
	Jinko::point operator+(Jinko::point const&);
	Jinko::point operator-(Jinko::point const&);

	Jinko::point operator+(Jinko::vector const&);
	Jinko::point operator-(Jinko::vector const&);

	point();
	point(float, float = 0);
};

class Jinko::Ipoint {
public:
	int x;
	int y;

	bool operator==(Jinko::point const&);
	Jinko::point operator+(Jinko::point const&);
	Jinko::point operator-(Jinko::point const&);

	Jinko::point operator+(Jinko::vector const&);
	Jinko::point operator-(Jinko::vector const&);

	Ipoint();
	Ipoint(float, float = 0);
	Ipoint(point);
};

class Jinko::line {
public:
	Jinko::point A;
	Jinko::point B;

	bool isOverlapping(Jinko::point);
	bool isOverlapping(float, float);

	line();
	line(point, point);
	line(float, float, float, float);
};

class Jinko::triangle {
public:
	Jinko::point A;
	Jinko::point B;
	Jinko::point C;

	bool isOverlapping(float, float);
	bool isOverlapping(float, float, float, float);
	bool isOverlapping(Jinko::point);
	bool isOverlapping(Jinko::point, Jinko::point);
	bool isOverlapping(Jinko::line);

	triangle();
	triangle(point, point, point);
};

class Jinko::quad {
public:
	Jinko::point A;
	Jinko::point B;
	Jinko::point C;
	Jinko::point D;

	bool isOverlapping(float, float);
	bool isOverlapping(Jinko::point);
	bool isOverlapping(Jinko::line);
	bool isOverlapping(Jinko::triangle);

	bool isOverlappingPartially(Jinko::line);
	bool isOverlappingPartially(Jinko::triangle);

	quad();
	quad(point, point, point, point);
};