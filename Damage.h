#pragma once
#include "Mob.h"
#include "Agnosia.h"
#include "GeneralFunctions.h"


class Agnosia::damage {
public:
	float trueDamage;
	float magicDamage;
	float physicalDamage;

	float magicPenetration;
	float physicalPenetration;

	void inflict(Agnosia::Mob*);

	Agnosia::damage operator+(Agnosia::damage const&);
	damage();
};