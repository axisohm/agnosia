#pragma once
#include "JinkoDependencies.h"
#include "AgnosiaDependencies.h"
#include "GeneralFunctions.h"
#include "Geometry.h"

#define MaxGridSizeX 50
#define MaxGridSizeY 50
#define MaxTilesInGrid 2500
#define MaxAmountOfCharasInWorld 5000

#define MaxChunksInMapX 10
#define MaxChunksInMapY 10


#define crossableFromLeft 1
#define crossableFromTop 2
#define crossableFromRight 4
#define crossableFromBottom 8

// Represents a tile in a grid.
class Agnosia::GridElement {
public:
	// General
	std::string name;
	std::string description;
	int height;

	// Environment
	int type; // For Debug only
	bool isBorder;
	bool isCrossable;
	// For destructible environment
	int eHealth;
	bool isDestructible;

	int crossability; // Uses bit-wise system (see "crossableFrom---" defined above)

	float bordersSize[4];

	ID2D1Bitmap* image;

	GridElement();
	GridElement(int); // Use this function to load a preset.
};

// Represents a playable grid
class Agnosia::Chunk {
private:

public:
	Agnosia::GridElement grid[MaxGridSizeX][MaxGridSizeY];
	Jinko::point dim;

	std::string mapString; // For test only
	void generateRandomV1(int, int, int = 0, int = 7, unsigned int = 4);

	//For testing only
	void outputDungeon();
	void generateDunStr();

	Chunk();
};

class Agnosia::Map {
public:
	std::chrono::system_clock::time_point updateTime;
	Agnosia::Chunk* chunks[MaxChunksInMapX][MaxChunksInMapY];
	bool loadedChunks[MaxChunksInMapX][MaxChunksInMapY];

	// Players/NPCs
	Agnosia::Mob* chara[MaxAmountOfCharasInWorld];

	void addChunk(Agnosia::Chunk*, Jinko::point);
	void addMob(Agnosia::Mob*);

	void updateMobs(std::chrono::system_clock::time_point);
	bool isCrossable(Agnosia::Mob*, Jinko::point, int); // int is relative to the direction from which the player comes from. See "crossableFrom---" defined above). Uses Mob* to determine if the

	Map();
};