#pragma once
#include "Mob.h"


void Agnosia::Mob::move(std::chrono::system_clock::time_point now, Jinko::vector move, Agnosia::Map* map) {
	move.normalize();
	float moveAmp = (now - this->lastUpdateTime).count(); //Microseconds
	moveAmp = moveAmp / pow(10, 6);
	Jinko::point inter;
	inter.x = this->position.x + (moveAmp * move.x);
	inter.y = this->position.y + (moveAmp * move.y);

	bool _isMovePossible = true;

	Jinko::Ipoint currentPos(floor(position.x), floor(position.y));

	Jinko::vector progress;

	Jinko::point p1, p2, sp;
	p1 = this->position;
	p2 = p1;
	sp = this->position;
	bool _ended = false;

	// Début boucle
	while((progress.getNorm() < move.getNorm()) && !_ended && _isMovePossible){
		p2.x = floor(p2.x + getSign(move.x));
			
		if (p2.x > inter.x) {
			p2.x = inter.x;
			_ended = true;
		}

		float deltaX = (p2.x - p1.x);
		float deltaY = (deltaX * move.y) / move.x;
		
		int ytrain = floor(p2.y);
		if (deltaY > 0) {
			bool ended = false;
			while (!ended) {
				ytrain++;
				if (ytrain < p1.y + deltaY) {
					ended = true;
				}
				else {
					if (!map->isCrossable(this, Jinko::point(floor(p1.x), floor(p2.y + ytrain)), crossableFromTop)) {
						_isMovePossible = false;
						deltaY = ytrain - 0.1;
					}
				}
			}
		}
		else if (deltaY < 0) {
			bool ended = false;
			while (!ended) {
				ytrain--;
				if (ytrain > p1.y + deltaY) {
					ended = true;
				}
				else {
					if (!map->isCrossable(this, Jinko::point(floor(p1.x), floor(p2.y + ytrain)), crossableFromBottom)) {
						_isMovePossible = false;
						deltaY = ytrain + 0.1;
					}
				}
			}
		}

		if (!_isMovePossible) {
			inter.y = ytrain;
			inter.x = (inter.y * (p2.x - position.x)) / (p2.y - position.y);
		}
		else {
			if (deltaX > 0) {
				if (!map->isCrossable(this, Jinko::point(floor(p2.x), floor(p2.y + deltaY)), crossableFromLeft)) {
					_isMovePossible = false;
					deltaX = deltaX - 0.1;
				}
			}
			else if (deltaX < 0) {
				if (!map->isCrossable(this, Jinko::point(floor(p2.x), floor(p2.y + deltaY)), crossableFromRight)) {
					_isMovePossible = false;
					deltaX = deltaX + 0.1;
				}
			}
		}


		sp.x = position.x + p1.x + deltaX;
		sp.y = position.y + p1.y + deltaY;
		p1 = p2;
		progress = p1 - this->position;
	}

	if (_isMovePossible) {
		this->position = inter;
	}
	else {
		this->position = sp;
	}
}
void Agnosia::Mob::init(std::chrono::system_clock::time_point now) {
	this->lastUpdateTime = now;
}

void Agnosia::Mob::update(std::chrono::system_clock::time_point now) {
	this->lastUpdateTime = now;
}

Agnosia::Mob::Mob(){
	this->name = "";
	this->description = "";

	this->maxHealth = 1;
	this->health = 1;

}
