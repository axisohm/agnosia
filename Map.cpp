#include "Map.h"


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * DUNGEON GRID * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// heightCenters is the amount of different height centers seeded at the generation.
void Agnosia::Chunk::generateRandomV1(int x, int y, int heightFloor, int heightCeil, unsigned int heightCenters) {
	this->dim.x = x; this->dim.y = y;
	// Raw generation
		// Generate height seeds
	int maxPoints = MaxGridSizeX * MaxGridSizeY;
	Jinko::point heightCentersPosition[MaxTilesInGrid];
	int heightCenterAltitude[MaxTilesInGrid];
	for (int i = 0; i < heightCenters; i++) {
		int X = random(0, x);
		int Y = random(0, y);
		int Z = random(heightFloor, heightCeil);
		heightCentersPosition[i].x = X;
		heightCentersPosition[i].y = Y;
		heightCenterAltitude[i] = Z;
		this->grid[x][y].height = Z;
	}
		//Generate altitude for all blocks
	if (heightCenters <= 0) {
		for (int ix = 0; ix < x; ix++) for (int iy = 0; iy < y; iy++) this->grid[x][y].height = heightFloor;
	}
	else {
		for (int ix = 0; ix < x; ix++) {
			for (int iy = 0; iy < y; iy++) {
				// Get distance from all height centers
				float value = 0;
				float weight = 0;
				float totalWeight = 0;
				Jinko::point here(ix, iy);
				bool isHeightCenter = false;
				int iW = 0;
				for (int i = 0; (i < heightCenters) && (!isHeightCenter); i++) {
					if ((heightCentersPosition[i].x == here.x) && (heightCentersPosition[i].y == here.y)) {
						isHeightCenter = true;
						iW = i;
					}
					else {
						Jinko::vector distance(heightCentersPosition[i] - here);
						weight = pow((1 / distance.getNorm()), 2);
						value = value + (weight * heightCenterAltitude[i]);
						totalWeight = totalWeight + weight;
					}
				}
				if (!isHeightCenter) {
					this->grid[ix][iy].height = round(value / totalWeight);
				}
				else {
					this->grid[ix][iy].height = heightCenterAltitude[iW];
				}
			}
		}
	}

	// Smoothening
}


//Testing only. Generates a file containing all info about the dungeon.
void Agnosia::Chunk::outputDungeon() {
	_wmkdir(L"./Test");
	_wmkdir(L"./Test/Dungeon");
	std::string dunStr;
	std::ofstream file("./Test/Dungeon/Test1.txt", std::ios::trunc); //ios::trunc is used to erase the previous content before editing the file
	dunStr = dunStr + "Dimensions : " + std::to_string(this->dim.x) + "x" + std::to_string(this->dim.y) + ".\n";
	dunStr = dunStr + "Altitude map :\n";
	int charaHeight = -1;
	for (int x = 0; x < this->dim.x; x++) {
		for (int y = 0; y < this->dim.y; y++) {
			dunStr = dunStr + std::to_string(this->grid[x][y].height) + " ";
		}
		dunStr = dunStr + "\n";
	}
	if (charaHeight > -1) dunStr = dunStr + "Player's height = " + std::to_string(charaHeight);
	dunStr = dunStr + "\n";
	dunStr = dunStr + "End of file.";
	file << dunStr;
	file.close();
	this->mapString = dunStr;
}

void Agnosia::Chunk::generateDunStr() {
	std::string dunStr;
	dunStr = dunStr + "Dimensions : " + std::to_string(this->dim.x) + "x" + std::to_string(this->dim.y) + ".\n";
	dunStr = dunStr + "Altitude map :\n";
	int charaHeight = -1;
	for (int x = 0; x < this->dim.x; x++) {
		for (int y = 0; y < this->dim.y; y++) {
			dunStr = dunStr + std::to_string(this->grid[x][y].height) + " ";
		}
		dunStr = dunStr + "\n";
	}
	if (charaHeight > -1) {
		dunStr = dunStr + "Player's height = " + std::to_string(charaHeight);
	}
	dunStr = dunStr + "\n";
	this->mapString = dunStr;
}


Agnosia::Chunk::Chunk() {
	this->dim.x = 0; this->dim.y = 0;
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * GRID ELEMENT * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //


Agnosia::GridElement::GridElement() {
	this->isBorder = false;
	this->isCrossable = true;
	this->eHealth = 1;
	this->isDestructible = false;
	this->crossability = crossableFromLeft + crossableFromTop + crossableFromRight + crossableFromBottom;
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * * * Map * * * * * * * * * * * * * * * * * * * //
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //


void Agnosia::Map::addChunk(Agnosia::Chunk* chunk, Jinko::point loc) {
	int x = floor(loc.x);
	int y = floor(loc.y);
	this->chunks[x][y] = chunk;
	this->loadedChunks[x][y] = true;
}

void Agnosia::Map::addMob(Agnosia::Mob* mob) {
	bool _isEnded = false;
	int i = 0;
	while (!_isEnded) {
		if (this->chara[i] == nullptr) _isEnded = true;
		else i++;
	}
	this->chara[i] = mob;
}

bool Agnosia::Map::isCrossable(Agnosia::Mob* mob, Jinko::point loc, int playerDir) {
	bool evaluated = false;
	bool value = true;
	int chunkX = floor(loc.x / MaxGridSizeX);
	int chunkY = floor(loc.y / MaxGridSizeY);
	int x = ((int)floor(loc.x) % MaxGridSizeX);
	int y = ((int)floor(loc.y) % MaxGridSizeY);

	if (!((chunkX > MaxChunksInMapX) || (chunkX < 0) || (chunkY > MaxChunksInMapY) || (chunkY < 0))) {

		if ((playerDir & crossableFromBottom) == crossableFromBottom) {
			evaluated = true;
			if (this->loadedChunks[chunkX][chunkY]) {
				if (!((this->chunks[chunkX][chunkY]->grid[x][y].crossability & crossableFromBottom) == crossableFromBottom)) value = false;
			}
			else value = false;
		}
		if ((playerDir & crossableFromRight) == crossableFromRight) {
			evaluated = true;
			if (this->loadedChunks[chunkX][chunkY]) {
				if (!((this->chunks[chunkX][chunkY]->grid[x][y].crossability & crossableFromRight) == crossableFromRight)) value = false;
			}
			else value = false;
		}
		if ((playerDir & crossableFromTop) == crossableFromTop) {
			evaluated = true;
			if (this->loadedChunks[chunkX][chunkY]) {
				if (!((this->chunks[chunkX][chunkY]->grid[x][y].crossability & crossableFromTop) == crossableFromTop)) value = false;
			}
			else value = false;
		}
		if ((playerDir & crossableFromLeft) == crossableFromLeft) {
			evaluated = true;
			if (this->loadedChunks[chunkX][chunkY]) {
				if (!((this->chunks[chunkX][chunkY]->grid[x][y].crossability & crossableFromLeft) == crossableFromLeft)) value = false;
			}
			else value = false;
		}
		if (!evaluated) value = false;
		return value;
	}
	else return false;
}

void Agnosia::Map::updateMobs(std::chrono::system_clock::time_point now) {

}

Agnosia::Map::Map() {
	for (int x = 0; x < MaxChunksInMapX; x++) for (int y = 0; y < MaxChunksInMapY; y++) {
		this->chunks[x][y] = nullptr;
		this->loadedChunks[x][y] = false;
	}
}